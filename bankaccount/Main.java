package bankaccount;

import java.util.Scanner;

/**
 * Класс Main для создания двух банковских аккаунтов для одного пользователя
 *
 * @author Tropanova N.S.
 */
public class Main {

        private static Scanner scanner = new Scanner(System.in);
        private static final Account ACCOUNT_SBERBANK = new Account("1524216793565289", "Nadezhda Tropanova", 700);
        private static final Account ACCOUNT_VTB = new Account("1234235753567267", "Nadezhda Tropanova", 5000);

        private static final Account.Card CARD_SBERBANK_MIR = ACCOUNT_SBERBANK.new Card("5742 2570 7636 7243");
        private static final Account.Card CARD_SBERBANK_MASTERCARD = ACCOUNT_SBERBANK.new Card("5382 5280 0536 7663");
        private static final Account.Card CARD_VTB_MIR = ACCOUNT_VTB.new Card("4793 7436 3523 7573");
    public static void main(String[] args) {
        int end;
        do {
            int chosenBank = choosingBank();
            int chosenCard = choosingCard(chosenBank);
            Account userWorkingNowBank = returnUserWorkingNowBank(chosenBank);
            Account.Card userWorkingNowCard = returnUserWorkingNowCard(chosenBank, chosenCard);

            System.out.println("1. Снять деньги\n" +
                    "2. Внести деньги");
            int withdrawOrDeposit = scanner.nextInt();

            int moneyToWithdrawOrDeposit = howMuchToWithdrawOrDeposit(withdrawOrDeposit);

            checkingBank(moneyToWithdrawOrDeposit, withdrawOrDeposit, userWorkingNowBank, userWorkingNowCard);

            System.out.println("\n1. Выполнить другое действие\n" +
                    "2. Выход");
            end = scanner.nextInt();
        } while (end == 1);
    }

    /**
     * Предоставляет выбор банка
     *
     * @return номер банка выбранного пользователем
     */
    private static int choosingBank() {
        System.out.println("Выберите банк:\n" +
                "1. Сбербанк\n" +
                "2. ВТБ");
        return scanner.nextInt();
    }

    /**
     * Предоставляет выбор карты банка
     *
     * @param chosenBank банк, выбранный пользователем
     * @return номер карты банка, выбранной пользователем
     */
    private static int choosingCard(int chosenBank) {
        System.out.println("Выберите карту: ");
        if (chosenBank == 1) {
            System.out.println("1. Sberbank Mir\n" +
                    "2. Sberbank Mastercard");
        }
        if (chosenBank == 2) {
            System.out.println("1. VTB Mir");
        }
        if (chosenBank > 2) {
            System.out.println("Карта не найдена");
            return -1;
        }
        return scanner.nextInt();
    }

    /**
     * Позволяет идентифицировать банк для работы со картами
     *
     * @param chosenBank банк, выбранный пользователем
     * @return банк, с которым пользователь будет работать в данный момент
     */
    private static Account returnUserWorkingNowBank(int chosenBank) {
        if (chosenBank == 1) {
            return ACCOUNT_SBERBANK;
        }
        if (chosenBank == 2) {
            return ACCOUNT_VTB;
        }
        return new Account("00000000", "Ошибка");
    }

    /**
     * Позволяет идентифицировать карту банка для работы со счетами
     *
     * @param chosenBank банк, который выбрал пользователь
     * @param chosenCard карта банка, которую выбрал пользователь
     * @return карту банка, с которой пользователь будет работать в данный момент
     */
    private static Account.Card returnUserWorkingNowCard(int chosenBank, int chosenCard) {
        if (chosenBank == 1) {
            if (chosenCard == 1) {
                return CARD_SBERBANK_MIR;
            }
            if (chosenCard == 2) {
                return CARD_SBERBANK_MASTERCARD;
            }
        }
        if (chosenBank == 2) {
            if (chosenCard == 1) {
                return CARD_VTB_MIR;
            }
        }
        return ACCOUNT_SBERBANK.new Card("00000000");
    }

    /**
     * Принимает значение желаемой суммы пользователя на пополнение/снятие
     *
     * @param withdrawOrDeposit действие пользователя с картами (пополнение/снятие)
     * @return сумма пополнения/снятия
     */
    private static int howMuchToWithdrawOrDeposit(int withdrawOrDeposit) {
        if (withdrawOrDeposit == 1) {
            System.out.print("Сколько нужно снять: ");
        }
        if (withdrawOrDeposit == 2) {
            System.out.print("Сколько нужно внести: ");
        }
        return scanner.nextInt();
    }

    /**
     * Сообщает пользователю результат его действий (пополнения/снятия) определенной суммы
     *
     * @param MoneyToWithdrawOrDeposit деньги для пополнения/снятия
     * @param withdrawOrDeposit        пополнение/снятие
     * @param userWorkingNowBank       банк, с которым в данный моент работает пользователь
     * @param userWorkingNowCard       карта банка, с которой в данный момент работает пользователь
     */
    private static void checkingBank(int MoneyToWithdrawOrDeposit, int withdrawOrDeposit, Account userWorkingNowBank, Account.Card userWorkingNowCard) {
        System.out.println("Ваш баланс: " + userWorkingNowBank.getAmount());

        if (withdrawOrDeposit == 1) {
            int tempWithdraw = userWorkingNowCard.withdraw(MoneyToWithdrawOrDeposit);

            if (tempWithdraw == -1) {
                System.out.println("Ошибка");
            }

            if (tempWithdraw < MoneyToWithdrawOrDeposit) {
                System.out.println("Недостаточно средств, возможно снять только = " + tempWithdraw);
                System.out.println("Баланс (" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            } else {
                System.out.println("Баланс (" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            }
        }

        if (withdrawOrDeposit == 2) {
            int tempTopUp = userWorkingNowCard.topUp(MoneyToWithdrawOrDeposit);

            if (tempTopUp == -1) {
                System.out.println("Ошибка (" + userWorkingNowBank.getOwner() + ")");
            }

            if (tempTopUp == -2) {
                System.out.println("Слишком много денег, НЕ БОЛЬШЕ 100К: " + MoneyToWithdrawOrDeposit);
                System.out.println("Баланс (" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            } else {
                System.out.println("Баланс(" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            }
        }
    }
}
