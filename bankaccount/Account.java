package bankaccount;

/**
 * Класс аккаунт
 *
 * @author Tropanova N.S.
 */
public class Account {
    private final String number;
    private final String owner;
    private int amount;

    Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    Account(String number, String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    int getAmount() {
        return amount;
    }

    /**
     * Метод, реализующий снятие определенной суммы с карты
     *
     * @param amountToWithdraw сумма для вывода
     * @return -1 при отрицательной сумме,
     * *         баланс при сумме больше, чем есть на балансе,
     * *         сумму снятия
     */
    private int withdraw(int amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return -1;
        }
        if (amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        amount = amount - amountToWithdraw;
        return amountToWithdraw;
    }

    /**
     * Пополнение баланса
     *
     * @param money сумма для пополнения
     * @return -1 при отрицательной сумме,
     * -2 при сумме большей чем 100к
     * сумму пополнения
     */
    private int deposit(int money) {
        if (money < 0) {
            return -1;
        }
        if (money > 100000) {
            return -2;
        }
        amount += money;
        return money;
    }

    /**
     * Метод, реализующий пополнения определенной суммы на карту
     *
     * @param money сумма для пополнения
     * @return сумма пополнения
     */
    int topUp(final int money) {
        return Account.this.deposit(money);
    }

    /**
     * Класс, демонстрирующий работу с картой
     *
     * @author Tropanova N.S.
     */
    public class Card {
        private final String number;

        Card(final String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }

        /**
         * Метод, реализующий снятие определенной суммы с карты
         *
         * @param amountToWithdraw сумма для вывода
         * @return сумма снятия
         */
        int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }
        /**
         * Метод, реализующий пополнения определенной суммы на карту
         *
         * @param money сумма для пополнения
         * @return сумма пополнения
         */
        int topUp(final int money) {
            return Account.this.deposit(money);
        }
    }
}

