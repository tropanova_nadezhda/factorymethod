package iceCreatParlorDecorator.decorators;

import iceCreatParlorDecorator.product.Component;

    public abstract class Decorator implements Component {
        private Component component;

        Decorator(Component component) {
            this.component = component;
        }

        abstract void afterTopping();

        @Override
        public void topping() {
            component.topping();
            afterTopping();
        }

}
