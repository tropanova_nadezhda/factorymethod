package iceCreatParlorDecorator.decorators;

import iceCreatParlorDecorator.product.Component;

public class NoTopping extends Decorator {
    public NoTopping(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" -  без топпинга");
    }
}
