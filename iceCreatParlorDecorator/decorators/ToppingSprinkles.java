package iceCreatParlorDecorator.decorators;

import iceCreatParlorDecorator.product.Component;

public class ToppingSprinkles extends Decorator {
    public ToppingSprinkles(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлен топпинг-посыпка");
    }
}
