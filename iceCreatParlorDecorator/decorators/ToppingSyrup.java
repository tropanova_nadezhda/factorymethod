package iceCreatParlorDecorator.decorators;

import iceCreatParlorDecorator.product.Component;

public class ToppingSyrup extends Decorator {
    public ToppingSyrup(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлен топпинг-сироп");
    }
}
