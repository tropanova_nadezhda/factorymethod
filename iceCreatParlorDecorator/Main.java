package iceCreatParlorDecorator;

import iceCreatParlorDecorator.decorators.NoTopping;
import iceCreatParlorDecorator.decorators.ToppingSprinkles;
import iceCreatParlorDecorator.decorators.ToppingSyrup;
import iceCreatParlorDecorator.product.Component;
import iceCreatParlorDecorator.product.Ice;
import iceCreatParlorDecorator.product.Smoothies;
import iceCreatParlorDecorator.product.Yogurt;

import java.util.Random;

/**
 * Класс для реализации процесса продажи мороженого, йогурта и смузи
 *
 * @author Tropanova N.S.
 */
public class Main {
    public static void main(String[] args) {
        Component ice;
        Component yogurt;
        Component smoothies;
        Random random = new Random();
        boolean showBorder = random.nextBoolean();
        if (showBorder) {
            ice = new ToppingSprinkles(new Ice());
            yogurt = new NoTopping(new Yogurt());
            smoothies = new ToppingSyrup(new Smoothies());
        } else {
            ice = new Ice();
            yogurt = new Yogurt();
            smoothies = new Smoothies();
        }

        ice.topping();
        yogurt.topping();
        smoothies.topping();

        ice = new ToppingSprinkles(new ToppingSyrup(new Ice()));
        ice.topping();

        yogurt = new ToppingSprinkles(new ToppingSprinkles(new Yogurt()));
        yogurt.topping();

        smoothies = new ToppingSprinkles(new ToppingSyrup(new Smoothies()));
        smoothies.topping();

    }
}